# Workshop Agenda

# Serverless Development - Use Cases und Praxis

## Serverless Services - Mehr als nur Lambda
## Use Cases - Was ist sinnvoll? 
## Frameworks - Finde Deine produktive Umgebung
## Local Testing - Welche Möglichkeiten habe ich?

# Infrastruktur mit CDK - Die neue Iteration

## Überblick und Setup
## Development Process
## Testing
## Demo und Beispiele
