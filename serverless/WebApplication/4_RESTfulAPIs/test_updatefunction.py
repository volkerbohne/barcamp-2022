import updatefunction
import json

with open("TestEvent", "r") as f:
    testevent = json.load(f)
context = {
    log_stream_name = "foo"
}

def test_create_event():
    assert updatefunction.handler(testevent, "context") == 200
