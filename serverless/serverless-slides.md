footer: Serverless Development - Use Cases und Praxis
slidenumbers: true

# Serverless Development - Use Cases und Praxis
https://gitlab.com/volkerbohne/barcamp-2022

---

# Über mich

---

## ?

> Serverless (zu Deutsch: Serverlos, ohne Server) ist ein Ausführungsmodell, bei dem der Cloud-Anbieter (AWS, Azure oder Google Cloud) für die Ausführung von Code verantwortlich ist, indem er die Ressourcen dynamisch zuweist. Es werden nur Kosten für die Zeit verrechnet, die der Code auch tatsächlich zum Ausführen braucht.
-- https://serverless-stack.com/

---

# Keine Server? 

## Doch, und alte Probleme
## Less ops

---

# Vendor Lock in

## Meh!
## Äpfel und Birnen

---

# Mache es!

+ Evaluation
+ Kleine Arbeitslasten
+ KISS
+ Event driven
+ Decoupled
+ Stateless

---

# Eher nicht machen

- Kein Monitoring / Tracing implementiert
- Heavy loads
-- Limits
-- Concurrency

---

# Storage
- S3
- EFS

---

# API Endpoints
- API Gateway
-- Rest
-- HTTP 
-- Websockets

---

# Compute
- Lambda
- Fargate

---

# Database
- DynamoDB
- Aurora 

---

# Authentication
Cognito
- Identity Provider und User Pools

---

# Containers

> AWS provides 17 ways to run containers and they all suck.
-- Corey Quinn

---

# Use Cases

- API
 HTTP API Gateway, Fargate, Relationale Datenbank 

---

# Real World Example :tm:
Nicht ganz ernst gemeint

---

# Frameworks / Dev Umgebung
 IDE
 AWS CloudFormation
 AWS SAM
 Serverless.com
 Hashicorp Terraform

---

# IDE
 Jetbrains
 Visual Studio Code
 Cloud9

---

# AWS CloudFormation
Wall of YAML (oder JSON)
JM2C: muss man nicht haben aber kennen.

---

# AWS SAM
Eigentlich CloudFormation (Transforms)
etwas kompakterer Code
native Unterstützung von AWS Resourcen

---

# Serverless.com
Provider agnostisch
[Serverless.com](http://www.serverless.com)
vieles nur mit Plugins realisierbar

---

# Hashicorp Terraform
Provider agnostisch
Für Serverless genauso deklarativ wie CloudFormation
Plus: Functions, Module
Terragrunt, Terratest und viel 3rd Party

---

# CDK CloudFormation Development Kit

IaC in Typescript, Python, Go ...
Constructs !

---

# CDK in the wild

CDK, CDKtf, CDK8s, projen

---

# I like

[Serverless Land](https://serverlessland.com)
[CDK Day](https://www.cdkday.com/)
Serverless Community on Twitter

---
