---
AWSTemplateFormatVersion: '2010-09-09'
Description: Resources for Wild Ryde rider photo processing workflow.
Outputs:
  FaceDetectionFunctionArn:
    Value:
      Fn::GetAtt:
      - FaceDetectionFunction
      - Arn
  FaceSearchFunctionArn:
    Value:
      Fn::GetAtt:
      - FaceSearchFunction
      - Arn
  IndexFaceFunctionArn:
    Value:
      Fn::GetAtt:
      - IndexFaceFunction
      - Arn
  NotificationPlaceholderFunctionArn:
    Value:
      Fn::GetAtt:
      - NotificationPlaceholderFunction
      - Arn
  PersistMetadataFunctionArn:
    Value:
      Fn::GetAtt:
      - PersistMetadataFunction
      - Arn
  RiderPhotoDDBTable:
    Value:
      Ref: RiderPhotoDDBTable
  RiderPhotoS3Bucket:
    Value:
      Ref: RiderPhotoS3Bucket
  StateMachineRole:
    Value:
      Fn::GetAtt:
      - StateMachineRole
      - Arn
  ThumbnailFunctionArn:
    Value:
      Fn::GetAtt:
      - ThumbnailFunction
      - Arn
  ThumbnailS3Bucket:
    Value:
      Ref: ThumbnailS3Bucket
Parameters:
  RekognitionCollectionID:
    AllowedPattern: '[a-zA-Z0-9_.-]+'
    Default: rider-photos
    Description: ID for the Rekognition collection used to index faces
    MaxLength: 255
    MinLength: 1
    Type: String
  TestImagesBucket:
    Default: wild-rydes-sfn-module-eu-west-1
    Description: S3 bucket containing the test images to copy over
    Type: String
  TestImagesPrefix:
    Default: test-images/
    Description: Key prefix for test images to copy over
    Type: String
Resources:
  CopyS3ObjectsFunction:
    Properties:
      CodeUri: src/lambda-functions/copy-s3-object
      Description: Copies objects from a source S3 bucket to a destination
      Handler: index.handler
      Policies:
        Statement:
        - Action:
          - s3:ListBucket
          - s3:GetObject
          Effect: Allow
          Resource:
          - Fn::Sub: arn:aws:s3:::${TestImagesBucket}
          - Fn::Sub: arn:aws:s3:::${TestImagesBucket}/${TestImagesPrefix}*
          Sid: SourceBucketReadAccess
        - Action:
          - s3:ListBucket
          - s3:ListBucketVersions
          - s3:GetBucketVersioning
          - s3:GetObject
          - s3:GetObjectVersion
          - s3:PutObject
          - s3:PutObjectAcl
          - s3:PutObjectVersionAcl
          - s3:DeleteObject
          - s3:DeleteObjectVersion
          - s3:CopyObject
          Effect: Allow
          Resource:
          - Fn::Sub: arn:aws:s3:::${RiderPhotoS3Bucket}
          - Fn::Sub: arn:aws:s3:::${RiderPhotoS3Bucket}/*
          - Fn::Sub: arn:aws:s3:::${ThumbnailS3Bucket}
          - Fn::Sub: arn:aws:s3:::${ThumbnailS3Bucket}/*
          Sid: DestBucketWriteAccess
      Runtime: python3.8
      Timeout: 120
    Type: AWS::Serverless::Function
  EmptyThumbnailBucket:
    Properties:
      Bucket:
        Ref: ThumbnailS3Bucket
      ServiceToken:
        Fn::GetAtt:
        - CopyS3ObjectsFunction
        - Arn
    Type: Custom::S3Objects
  FaceDetectionFunction:
    Properties:
      CodeUri: src/lambda-functions/face-detection
      Description: Use Amazon Rekognition to detect faces
      Handler: index.handler
      MemorySize: 256
      Policies:
        Statement:
        - Action:
          - s3:GetObject
          Effect: Allow
          Resource: '*'
          Sid: ReadFromS3Bucket
        - Action:
          - rekognition:DetectFaces
          Effect: Allow
          Resource: '*'
          Sid: RekognitionFace
      Runtime: nodejs12.x
      Timeout: 60
    Type: AWS::Serverless::Function
  FaceSearchFunction:
    Properties:
      CodeUri: src/lambda-functions/face-search
      Description: Use Amazon Rekognition to check if the face is already in the collection
      Environment:
        Variables:
          REKOGNITION_COLLECTION_ID:
            Ref: RekognitionCollectionID
      Handler: index.handler
      MemorySize: 256
      Policies:
        Statement:
        - Action:
          - s3:GetObject
          Effect: Allow
          Resource:
            Fn::Sub: arn:aws:s3:::${RiderPhotoS3Bucket}/*
          Sid: ReadFromS3Bucket
        - Action:
          - rekognition:SearchFacesByImage
          Effect: Allow
          Resource: '*'
          Sid: SearchFace
      Runtime: nodejs12.x
      Timeout: 60
    Type: AWS::Serverless::Function
  IndexFaceFunction:
    Properties:
      CodeUri: src/lambda-functions/index-face/
      Description: Index the photo into Rekognition collection
      Environment:
        Variables:
          REKOGNITION_COLLECTION_ID:
            Ref: RekognitionCollectionID
      Handler: index.handler
      MemorySize: 256
      Policies:
        Statement:
        - Action:
          - s3:GetObject
          Effect: Allow
          Resource:
            Fn::Sub: arn:aws:s3:::${RiderPhotoS3Bucket}/*
          Sid: ReadFromS3Bucket
        - Action:
          - rekognition:IndexFaces
          Effect: Allow
          Resource: '*'
          Sid: SearchFace
      Runtime: nodejs12.x
      Timeout: 60
    Type: AWS::Serverless::Function
  NotificationPlaceholderFunction:
    Properties:
      CodeUri: src/lambda-functions/mock-notification
      Description: mock notification sender
      Handler: index.handler
      MemorySize: 256
      Runtime: nodejs12.x
      Timeout: 60
    Type: AWS::Serverless::Function
  PersistMetadataFunction:
    Properties:
      CodeUri: src/lambda-functions/persist-metadata
      Description: Save metadata of the photo to DynamoDB table
      Environment:
        Variables:
          RIDER_PHOTOS_DDB_TABLE:
            Ref: RiderPhotoDDBTable
      Handler: index.handler
      MemorySize: 256
      Policies:
        Statement:
        - Action:
          - dynamodb:PutItem
          Effect: Allow
          Resource:
            Fn::Sub: arn:aws:dynamodb:${AWS::Region}:${AWS::AccountId}:table/${RiderPhotoDDBTable}
          Sid: WriteToRiderPhotoDDBTable
      Runtime: nodejs12.x
      Timeout: 60
    Type: AWS::Serverless::Function
  PopulateTestImages:
    Properties:
      Bucket:
        Ref: RiderPhotoS3Bucket
      ServiceToken:
        Fn::GetAtt:
        - CopyS3ObjectsFunction
        - Arn
      SourceBucket:
        Ref: TestImagesBucket
      SourcePrefix:
        Fn::Sub: ${TestImagesPrefix}
    Type: Custom::S3Objects
  RiderPhotoDDBTable:
    Properties:
      AttributeDefinitions:
      - AttributeName: Username
        AttributeType: S
      KeySchema:
      - AttributeName: Username
        KeyType: HASH
      ProvisionedThroughput:
        ReadCapacityUnits: '3'
        WriteCapacityUnits: '3'
    Type: AWS::DynamoDB::Table
  RiderPhotoS3Bucket:
    Properties:
      CorsConfiguration:
        CorsRules:
        - AllowedHeaders:
          - '*'
          AllowedMethods:
          - PUT
          - GET
          - POST
          - HEAD
          AllowedOrigins:
          - '*'
          ExposedHeaders:
          - ETag
    Type: AWS::S3::Bucket
  StateMachineRole:
    Properties:
      AssumeRolePolicyDocument:
        Statement:
        - Action:
          - sts:AssumeRole
          Effect: Allow
          Principal:
            Service:
              Fn::Sub: states.${AWS::Region}.amazonaws.com
        Version: '2012-10-17'
      Path: /WildRydes/
      Policies:
      - PolicyDocument:
          Statement:
          - Action:
            - lambda:InvokeFunction
            Effect: Allow
            Resource: '*'
            Sid: InvokeLambda
          Version: '2012-10-17'
        PolicyName: InvokeLambda
      - PolicyDocument:
          Statement:
          - Action:
            - SNS:Publish
            Effect: Allow
            Resource: '*'
            Sid: PublishToSNS
          Version: '2012-10-17'
        PolicyName: PublishToSNS
    Type: AWS::IAM::Role
  ThumbnailFunction:
    Properties:
      CodeUri: src/lambda-functions/thumbnail
      Environment:
        Variables:
          MAX_HEIGHT: 300
          MAX_WIDTH: 300
          THUMBNAIL_BUCKET:
            Ref: ThumbnailS3Bucket
      Handler: index.handler
      MemorySize: 1536
      Policies:
        Statement:
        - Action:
          - s3:PutObject
          Effect: Allow
          Resource:
            Fn::Sub: arn:aws:s3:::${ThumbnailS3Bucket}/*
          Sid: WritetoS3ThumbnailBucket
        - Action:
          - s3:GetObject
          Effect: Allow
          Resource:
            Fn::Sub: arn:aws:s3:::${RiderPhotoS3Bucket}/*
          Sid: ReadFromS3
      Runtime: nodejs12.x
      Timeout: 300
    Type: AWS::Serverless::Function
  ThumbnailS3Bucket:
    Properties:
      CorsConfiguration:
        CorsRules:
        - AllowedHeaders:
          - '*'
          AllowedMethods:
          - PUT
          - GET
          - POST
          - HEAD
          AllowedOrigins:
          - '*'
          ExposedHeaders:
          - ETag
    Type: AWS::S3::Bucket
Transform: AWS::Serverless-2016-10-31
