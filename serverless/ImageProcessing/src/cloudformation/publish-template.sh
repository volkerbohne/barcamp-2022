#!/bin/bash

cd ../lambda-functions/thumbnail
npm install
echo "success installing npm dependencies!"
cd ../copy-s3-object
pip3 install -r requirements.txt -t .
echo "success installing python dependencies!"
cd ../../cloudformation

listOfRegions="eu-west-1"

for region in $listOfRegions
do
  bucket="$(aws sts get-caller-identity --query "Account" --output text)-workshop-codebucket-${region}"
  echo $bucket
  aws cloudformation package --s3-bucket ${bucket} --region ${region} --template ./module-setup.yaml --output-template-file setup-sam-transformed-${region}.yaml --s3-prefix ImageProcessing
  aws s3 cp setup-sam-transformed-${region}.yaml s3://${bucket}/ImageProcessing/setup-sam.yaml
  echo "deployed to $bucket"
done
